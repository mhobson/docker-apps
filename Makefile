DIRS = base media-util transfer-util

all:
	$(MAKE) all -C base
	$(foreach DIR, $(DIRS), $(MAKE) all -C $(DIR);)

all_nc:
	$(MAKE) all_nc -C base
	$(foreach DIR, $(DIRS), $(MAKE) all_nc -C $(DIR);)

clean:
	$(foreach DIR, $(DIRS), $(MAKE) clean -C $(DIR);)
