# README

Miscellaneous Docker images for Linux programs.

## Base

The base image for other builds. Based on Alpine, with a few generic nicities.

## Media utilities

The `media-util` image adds things for grabbing media downloads, namely `yt-dlp`
and ``ffmpeg`.

## Transfer utilities

The `transfer-util` image includes extra download clients like `aria2` and
additional file-management functions like a specialized `fdupes` CLI to break
large filesystems into smaller chunks for deduplication.

### FUSE

In order to use FUSE-based utilities, remember to use these Docker options:

```sh
--cap-add SYS_ADMIN --device /dev/fuse
```
